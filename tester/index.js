import axios from "axios"
import * as dotenv from 'dotenv'

dotenv.config()

const graphql_url = process.env.URL_GRAPH
const graphql_port = process.env.PORT_GRAPH

axios.post(`http://${graphql_url}:${graphql_port}/graphql`, { query: "{ hello }" }).then(
  res => console.log(res.data)
).catch(err => console.error(err))