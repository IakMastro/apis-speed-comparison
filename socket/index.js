import express  from "express"
import { Server } from 'socket.io'

const app = express()
const io = new Server()
const url = process.env.SOCKET_URL
const port = process.env.SOCKET_PORT

app.get('/', (req, res) => {
  console.log('Connected to socket.io server')
})

io.on('connection', (socket) => {
  console.log('a user connected')
})

app.listen(port, () => {
  console.log(`Socket.io API listening at http://${url}:${port}`)
})
