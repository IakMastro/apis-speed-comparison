import express from 'express'
const app = express()
const url = process.env.REST_URL
const port = process.env.REST_PORT

app.get('/', (req, res) => {
  res.send('Hello World!')
})

app.listen(port, () => {
  console.log(`Rest API listening at http://${url}:${port}`)
})
